package com.example.demoswipe;

import android.content.Context;
import android.content.res.Configuration;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;

public class OnSwipeListener implements View.OnTouchListener
{
    private boolean dragHorizontal = false;
    private boolean dragVertical = false;
    private boolean dragSnapBack = false;
    private boolean animated = true;
    private boolean exitScreenOnSwipe = false;
    private long animationDelay = 500;
    private float dragSnapThreshold = 50;
    private float swipeDistanceThreshold = 100;
    private float swipeVelocityThreshold = 100;
    private float dragPrevX;
    private float dragPrevY;
    private GestureDetector gestureDetector = null;
    private Impl swiper = null;
    private View draggedView = null;

    public OnSwipeListener(Context context)
    {
        gestureDetector = new GestureDetector(context, new GestureListener());
        if (context instanceof Impl)
            swiper = (Impl) context;
    }

    public void onSwipeLeft(float distance)
    {
        if (swiper != null)
            swiper.onSwipeLeft(distance);
    }

    public void onSwipeRight(float distance)
    {
        if (swiper != null)
            swiper.onSwipeRight(distance);
    }

    public void onSwipeUp(float distance)
    {
        if (swiper != null)
            swiper.onSwipeUp(distance);
    }

    public void onSwipeDown(float distance)
    {
        if (swiper != null)
            swiper.onSwipeDown(distance);
    }

    public final boolean onTouch(View view, MotionEvent event)
    {
        if (view != null)
            draggedView = view;
        boolean gesture = gestureDetector.onTouchEvent(event);
        int action = event.getAction();
        if (dragHorizontal || dragVertical)
            action = MotionEvent.ACTION_POINTER_DOWN;
        else if (action == MotionEvent.ACTION_MOVE)
        {
            float dragCurrX = event.getRawX();
            float dragCurrY = event.getRawY();
            if (dragHorizontal)
                view.setTranslationX(view.getTranslationX() + dragCurrX - dragPrevX);
            if (dragVertical)
                view.setTranslationY(view.getTranslationY() + dragCurrY - dragPrevY);
        }
        else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_POINTER_UP)
        {
            if (dragSnapBack)
            {
                float dx = event.getRawX() - dragPrevX;
                float dy = event.getRawY() - dragPrevY;
                boolean shouldDoX = Math.abs(dx) <= dragSnapThreshold || dragSnapThreshold <= 0;
                boolean shouldDoY = Math.abs(dy) <= dragSnapThreshold || dragSnapThreshold <= 0;
                if (animated)
                {
                    ViewPropertyAnimator anim = view.animate();
                    if (shouldDoX)
                        anim.translationX(0);
                    if (shouldDoY)
                        anim.translationY(0);
                    anim.setDuration(animationDelay);
                    anim.start();
                }
                else
                {
                    if (shouldDoX)
                        view.setTranslationX(0);
                    if (shouldDoY)
                        view.setTranslationY(0);
                }
            }
            dragPrevX = event.getRawX();
            dragPrevY = event.getRawY();
        }
        return gesture;
    }

    public OnSwipeListener setDistanceThreshold(float px)
    {
        swipeDistanceThreshold = px;
        return this;
    }

    public OnSwipeListener setVelocityThreshold(float px)
    {
        swipeVelocityThreshold = px;
        return this;
    }

    public OnSwipeListener setDragSnapThreshold(float px)
    {
        dragSnapThreshold = px;
        if (dragSnapThreshold > 0)
            setDragSnapBack(true);
        return this;
    }

    public OnSwipeListener setAnimationDelay(long ms)
    {
        animationDelay = ms;
        setAnimated(animationDelay > 0);
        return this;
    }

    public OnSwipeListener setExitScreenOnSwipe(boolean exit)
    {
        exitScreenOnSwipe = exit;
        return this;
    }

    public OnSwipeListener setDragHorizontal(boolean drag)
    {
        dragHorizontal = drag;
        return this;
    }

    public OnSwipeListener setDragVertical(boolean drag)
    {
        dragVertical = drag;
        return this;
    }

    public OnSwipeListener setDragSnapBack(boolean snap)
    {
        dragSnapBack = snap;
        return this;
    }

    public OnSwipeListener setAnimated(boolean anim)
    {
        animated = anim;
        return this;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onDown(MotionEvent e)
        {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float vx, float vy)
        {
            float dx = e2.getRawX() - e1.getRawX();
            float dy = e2.getRawY() - e1.getRawY();
            Configuration config = draggedView.getContext().getApplicationContext().getResources().getConfiguration();
            int screenWidth = config.screenWidthDp;
            int screenHeight = config.screenHeightDp;
            if (Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > swipeDistanceThreshold && Math.abs(vx) > swipeVelocityThreshold)
            {
                if (dx > 0)
                {
                    onSwipeRight(dx);
                    dragEdgeHelper(screenWidth * 2, true, 0, false);
                }
                else
                {
                    onSwipeLeft(-dx);
                    dragEdgeHelper(-screenWidth, true, 0, false);
                }
                return true;
            }
            return false;
        }
    }

    private void dragEdgeHelper(float tx, boolean useTX, float ty, boolean useTY)
    {
        if (exitScreenOnSwipe && draggedView != null)
        {
            if (animated)
            {
                ViewPropertyAnimator anim = draggedView.animate().setDuration(animationDelay);
                if (useTX)
                    anim.translationX(tx);
                if (useTY)
                    anim.translationY(ty);
                anim.start();
            }
            else
                draggedView.setVisibility(View.INVISIBLE);
        }
    }

    public static interface Impl
    {
        void onSwipeLeft(float distance);
        void onSwipeRight(float distance);
        void onSwipeUp(float distance);
        void onSwipeDown(float distance);
    }
}
