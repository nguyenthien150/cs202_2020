package com.example.ungdungquanlychitieu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    EditText edtuser, edtpassword;
    Button btndangnhap, btndangki, btnthoat;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        controlButton();
    }

    private void controlButton()
    {
        btnthoat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Bạn có chắc muốn thoát không?");
                builder.setMessage("");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which)
                    {
                        onBackPressed();
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which)
                    {
                    }
                });
                builder.show();
            }
        });
        btndangnhap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String ten, mk;
                ten = edtuser.getText().toString();
                mk = edtpassword.getText().toString();
                if (ten.length() == 0 || mk.length() == 0)
                    Toast.makeText(MainActivity.this, "Vui lòng nhập lại thông tin!", Toast.LENGTH_SHORT).show();
                else
                {
                    if (ten.equals("phuc") && mk.equals("nguyen"))
                    {
                        Toast.makeText(MainActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
        btndangki.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Đăng kí");
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dangki);
                final EditText edtdkuser = dialog.findViewById(R.id.edtdkuser);
                final EditText edtdkpassword = dialog.findViewById(R.id.edtdkpassword);
                Button btndkhuy = dialog.findViewById(R.id.btndkhuy);
                Button btndkxacnhan = dialog.findViewById(R.id.btndkxacnhan);
                btndkxacnhan.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        String mk, ten;
                        ten = edtdkuser.getText().toString();
                        mk = edtdkpassword.getText().toString();
                        edtuser.setText(ten);
                        edtpassword.setText(mk);
                        Toast.makeText(MainActivity.this, "Đăng kí thành công", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
                btndkhuy.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });
    }

    private void Anhxa()
    {
        edtuser = findViewById(R.id.edtuser);
        edtpassword = findViewById(R.id.edtpassword);
        btndangnhap = findViewById(R.id.btndangnhap);
        btndangki = findViewById(R.id.btndangki);
        btnthoat = findViewById(R.id.btnthoat);
    }
}
